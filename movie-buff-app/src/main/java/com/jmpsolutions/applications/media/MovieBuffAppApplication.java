package com.jmpsolutions.applications.media;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieBuffAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieBuffAppApplication.class, args);
	}

}
